FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install python3 -y &&\
    apt-get install python3-pip -y
WORKDIR /app
COPY . .

RUN pip3 install -r requirements.txt

CMD ["python3", "app.py"]